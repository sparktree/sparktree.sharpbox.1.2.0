NUnit - Testdata for SharpBox
------------------------------

SharpBox is using NUnit for doing integration test. The following directories has to be copied on the following locations to execute our NUnit-Tests:

- ServerData --> In the root of your storage services (DropBox or WebDav)
- ClientData --> In SystemDrive\ClientData

