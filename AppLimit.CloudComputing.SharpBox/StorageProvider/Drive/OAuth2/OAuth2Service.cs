﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using AppLimit.CloudComputing.SharpBox.Common.Net;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.OAuth2
{
    internal class OAuth2Service 
    {
        public const String HeaderAuthorization = "Bearer {0}";

        public static WebRequest GetWebRequest(string url, string method, GoogleDriveTokenStorage credentials, NameValueCollection parameters = null)
        {
            //String query = HttpUtilityEx.ConstructQueryString(new NameValueCollection() { { "key", "AIzaSyBDu83AAPipb6uI0jiQqOI4lbk9REIN85c" } });
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = method.ToUpper();

            request.Headers.Add("Authorization", String.Format(HeaderAuthorization, credentials.AccessToken));

            //request.UserAgent = "Stripe.net (https://github.com/jaymedavis/stripe.net)";

            return request;
        }


        public static T ExecuteWebRequest<T>(WebRequest webRequest, out Int32 statusCode) where T :class 
        {
            try
            {
                using (var response = webRequest.GetResponse())
                {
                    String retString =  ReadStream(response.GetResponseStream());
                    statusCode = (int)HttpStatusCode.OK;
                    return JsonConvert.DeserializeObject<T>(retString);

                }
            }
            catch (WebException webException)
            {
                if (webException.Response != null)
                {
                    String retString = ReadStream(webException.Response.GetResponseStream());
                    
                    statusCode = (int)(((HttpWebResponse)webException.Response).StatusCode);
                    return retString as T;
                }

                throw;
            }
        }

        private static string ReadStream(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.UTF8))
            {
                return reader.ReadToEnd();
            }
        }



        private static string ConstructQueryString(NameValueCollection parameters)
        {
            var sb = new StringBuilder();

            foreach (String name in parameters)
                sb.Append(String.Concat(name, "=", System.Web.HttpUtility.UrlEncode(parameters[name]), "&"));

            if (sb.Length > 0)
                return sb.ToString(0, sb.Length - 1);

            return String.Empty;
        } 

    }
}
