﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using AppLimit.CloudComputing.SharpBox.Common.Net;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive
{
    static public class GoogleDriveStorageProviderTools
    {
        /// <summary>
        /// This method retrieves a new auth code from the google drive servers
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="ConsumerKey">The consumer key.</param>
        /// <param name="ConsumerSecret">The consumer secret.</param>
        /// <param name="redirectUri">The redirect URI.</param>
        /// <param name="forcePrompt">The force prompt.</param>
        /// <returns></returns>
        static public String GetGoogleDriveAuthorizationUrl(GoogleDriveProviderConfiguration configuration, String ConsumerKey, String ConsumerSecret, Boolean forcePrompt = false)
        {
            //configuration.AuthorizationCodeUrl;

            //Parameters set from https://developers.google.com/accounts/docs/OAuth2WebServer
            //TODO: get valid client_id from webconfig.
            NameValueCollection authParameters = new NameValueCollection();
            authParameters.Add("response_type", "code");
            authParameters.Add("client_id", ConsumerKey);
            
            authParameters.Add("scope", configuration.Scope);
            authParameters.Add("access_type","offline");
            
            authParameters.Add("redirect_uri", configuration.AuthorizationCallBack.ToString());


            authParameters.Add("approval_prompt", forcePrompt ? "force" : "auto");

            return configuration.AuthorizationCodeUrl + "?" + HttpUtilityEx.ConstructQueryString(authParameters);
            
        }

        /// <summary>
        /// Exchanges the auth code for access token.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="code">The code.</param>
        /// <param name="ConsumerKey">The consumer key.</param>
        /// <param name="ConsumerSecret">The consumer secret.</param>
        /// <param name="redirectUri">The redirect URI.</param>
        /// <returns></returns>
        public static GoogleDriveTokenStorage ExchangeAuthCodeForAccessToken(GoogleDriveProviderConfiguration configuration, GoogleDriveAuthCode code, String ConsumerKey, String ConsumerSecret)
        {
            if(String.Compare(code.Code,"access_denied", StringComparison.OrdinalIgnoreCase) == -1)
            {
                throw new HttpException();
            }
            NameValueCollection authParameters = new NameValueCollection();
            authParameters.Add("code", code.Code);
            authParameters.Add("client_id", ConsumerKey);
            authParameters.Add("client_secret", ConsumerSecret);
            authParameters.Add("redirect_uri", configuration.AuthorizationCallBack.ToString());
            authParameters.Add("grant_type", "authorization_code");

            WebRequest request = WebRequest.Create(configuration.AccessTokenUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            
            using(Stream postStream = request.GetRequestStream())
            {
                UTF8Encoding encoding = new UTF8Encoding();
                String message = HttpUtilityEx.ConstructQueryString(authParameters);
                postStream.Write(encoding.GetBytes(message), 0, message.Length);
            }

            
            using(WebResponse response = request.GetResponse())
            {
                
                using (Stream respData = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respData, Encoding.UTF8);
                    String responseString = reader.ReadToEnd();
                    GoogleDriveTokenStorage tokenStorage = JsonConvert.DeserializeObject<GoogleDriveTokenStorage>(responseString);
                    return tokenStorage;
                }
            }
        }

        /// <summary>
        /// Exchanges the long-lived refresh token for a short-lived access token.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <param name="token">The token.</param>
        /// <param name="ConsumerKey">The consumer key.</param>
        /// <param name="ConsumerSecret">The consumer secret.</param>
        /// <returns></returns>
        public static GoogleDriveTokenStorage ExchangeRefreshTokenForAccessToken(GoogleDriveProviderConfiguration configuration, GoogleDriveTokenStorage token, String ConsumerKey, String ConsumerSecret)
        {
            NameValueCollection authParameters = new NameValueCollection();
            authParameters.Add("refresh_token", token.RefreshToken);
            authParameters.Add("client_id", ConsumerKey);
            authParameters.Add("client_secret", ConsumerSecret);
            authParameters.Add("grant_type", "refresh_token");

            WebRequest request = WebRequest.Create(configuration.AccessTokenUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            using (Stream postStream = request.GetRequestStream())
            {
                UTF8Encoding encoding = new UTF8Encoding();
                String message = HttpUtilityEx.ConstructQueryString(authParameters);
                postStream.Write(encoding.GetBytes(message), 0, message.Length);
            }


            using (WebResponse response = request.GetResponse())
            {

                using (Stream respData = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respData, Encoding.UTF8);
                    String responseString = reader.ReadToEnd();
                    GoogleDriveTokenStorage tokenStorage = JsonConvert.DeserializeObject<GoogleDriveTokenStorage>(responseString);
                    return tokenStorage;
                }
            }
        }





    }
}
