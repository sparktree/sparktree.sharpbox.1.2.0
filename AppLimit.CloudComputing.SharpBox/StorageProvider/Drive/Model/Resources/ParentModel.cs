﻿using System;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources
{
    internal class ParentModel : IBaseModel
    {
        [JsonProperty(PropertyName = "kind")]
        public String Kind { get; set; }
        
        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "selfLink")]
        public String SelfLink { get; set; }

        [JsonProperty(PropertyName = "parentLink")]
        public String ParentLink { get; set; }

        [JsonProperty(PropertyName = "isRoot")]
        public Boolean IsRoot { get; set; }
       
    }
}
