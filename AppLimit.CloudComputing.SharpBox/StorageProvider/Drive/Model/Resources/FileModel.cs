﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources
{
    internal class FileModel : IBaseModel
    {
        [JsonProperty(PropertyName = "kind")]
        public String Kind { get; set; }

        [JsonProperty(PropertyName = "id")]
        public String Id { get; set; }

        [JsonProperty(PropertyName = "etag")]
        public String Etag { get; set; }

        [JsonProperty(PropertyName = "selfLink")]
        public String SelfLink { get; set; }

        [JsonProperty(PropertyName = "webContentLink")]
        public String WebContentLink { get; set; }


        [JsonProperty(PropertyName = "alternateLink")]
        public String AlternateLink { get; set; }

        [JsonProperty(PropertyName = "embedLink")]
        public String EmbedLink { get; set; }

        [JsonProperty(PropertyName = "thumbnailLink")]
        public String ThumbnailLink { get; set; }

        [JsonProperty(PropertyName = "title")]
        public String Title { get; set; }

        [JsonProperty(PropertyName = "mimeType")]
        public String MimeType { get; set; }

        [JsonProperty(PropertyName = "description")]
        public String Description { get; set; }


        [JsonProperty(PropertyName = "createdDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty(PropertyName = "modifiedDate")]
        public DateTime ModifiedDate { get; set; }

        [JsonProperty(PropertyName = "parents")]
        public List<ParentModel> Parents { get; set; }

        [JsonProperty(PropertyName = "quotaBytesUsed")]
        public String QuotaBytesUsed { get; set; }


        [JsonProperty(PropertyName = "downloadUrl")]
        public String DownloadUrl { get; set; }

        [JsonProperty(PropertyName = "exportLinks")]
        public Dictionary<String,String> ExportLinks { get; set; }

        [JsonProperty(PropertyName = "originalFilename")]
        public String OriginalFilename { get; set; }

        [JsonProperty(PropertyName = "fileExtension")]
        public String FileExtension { get; set; }

        [JsonProperty(PropertyName = "md5Checksum")]
        public String Md5Checksum { get; set; }

        [JsonProperty(PropertyName = "fileSize")]
        public long FileSize { get; set; }
        
    }
}
