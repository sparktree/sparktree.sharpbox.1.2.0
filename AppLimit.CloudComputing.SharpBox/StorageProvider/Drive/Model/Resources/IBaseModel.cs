﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources
{
    internal interface IBaseModel
    {
        String Kind { get; set; }

        String Id { get; set; }

        String SelfLink { get; set; }

    }
}
