﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Logic;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive
{
    public class GoogleDriveStorageProvider : GenericStorageProvider
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="GoogleDriveStorageProvider" /> class.
        /// </summary>
        public GoogleDriveStorageProvider()
            : base(new GoogleDriveStorageProviderService())
        { 
        }


        /// <summary>
        /// This methid returns the root node of the virtual filesystem which 
        /// is abstracted by SharpBox
        /// </summary>
        /// <returns></returns>
        public override ICloudDirectoryEntry GetRoot()
        {
            return _Service.RequestResource(_Session, "root", null) as ICloudDirectoryEntry;
        }


    }
}
