﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive
{
    public class GoogleDriveBaseTokenInformation
    {
        /// <summary>
        /// ConsumerKey of the DropBox application, provided by the 
        /// DropBox-Developer-Portal
        /// </summary>
        public String ConsumerKey { get; set; }

        /// <summary>
        /// ConsumerSecret of the DropBox application, provided by the 
        /// DropBox-Developer-Portal
        /// </summary>
        public String ConsumerSecret { get; set; }
    }
}
