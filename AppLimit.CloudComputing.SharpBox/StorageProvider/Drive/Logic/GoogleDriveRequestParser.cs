﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Web;
using AppLimit.CloudComputing.SharpBox.Common.IO;
using AppLimit.CloudComputing.SharpBox.Common.Net;
using AppLimit.CloudComputing.SharpBox.Exceptions;
using AppLimit.CloudComputing.SharpBox.StorageProvider.API;
using AppLimit.CloudComputing.SharpBox.StorageProvider.BaseObjects;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Resources;
using AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Model.Response;


namespace AppLimit.CloudComputing.SharpBox.StorageProvider.Drive.Logic
{
    internal static class GoogleDriveRequestParser
    {

        public const String PropFolderId = "PROP_FOLDER_ID";
        public const String PropChildId = "PROP_CHILD_ID";
        public const String PropFileId = "PROP_FILE_ID";
        
        
        public const String PropFileName = "PROP_FILE_NAME";
        public const String PropMimeType = "PROP_MIMETYPE";
        public const String PropParents = "PROP_PARENTS";

        public static BaseFileEntry CreateObjectsFromFileModel(IBaseModel driveModel, IStorageProviderService service, IStorageProviderSession session)
        {
            return UpdateObjectFromFileModel(driveModel, null, service, session);
        }

        public static BaseFileEntry UpdateObjectFromFileModel(IBaseModel driveModel, ICloudFileSystemEntry objectToUpdate, IStorageProviderService service, IStorageProviderSession session)
        {

            Boolean isDir = false;
            if(driveModel is FileModel)
            {
                FileModel fileModel = driveModel as FileModel;
                isDir = String.Compare(fileModel.MimeType, "application/vnd.google-apps.folder", StringComparison.InvariantCultureIgnoreCase) == 0;

            }
            

            // create the entry
            ICloudFileSystemEntry dbentry = null;
            Boolean bEntryOk = false;

            if (isDir)
            {
                if (objectToUpdate == null)
                    dbentry = new BaseDirectoryEntry("Name", 0, DateTime.Now, service, session);
                else
                    dbentry = objectToUpdate as BaseDirectoryEntry;

                bEntryOk = BuildDirectoryEntry(dbentry as BaseDirectoryEntry, driveModel, service, session);
            }
            else
            {
                if (objectToUpdate == null)
                    dbentry = new BaseFileEntry("Name", 0, DateTime.Now, service, session);
                else
                    dbentry = objectToUpdate;

                bEntryOk = BuildFileEntry(dbentry as BaseFileEntry, driveModel);
            }
            
            //// parse the childs and fill the entry as self
            //if (!bEntryOk)
            //    throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotContactStorageService);

            //// set the is deleted flag
            //try
            //{
            //    // try to read the is_deleted property
            //    dbentry.IsDeleted = jc.GetBooleanProperty("is_deleted");
            //}
            //catch (Exception)
            //{
            //    // the is_deleted proprty is missing (so it's not a deleted file or folder)
            //    dbentry.IsDeleted = false;
            //}

            // return the child
            return dbentry as BaseFileEntry;
        }

        private static Boolean BuildFileEntry(BaseFileEntry fileEntry, IBaseModel driveModel)
        {
            /*
             *  "revision": 29251,
                "thumb_exists": false,
                "bytes": 37941660,
                "modified": "Tue, 01 Jun 2010 14:45:09 +0000",
                "path": "/Public/2010_06_01 15_53_48_336.nvl",
                "is_dir": false,
                "icon": "page_white",
                "mime_type": "application/octet-stream",
                "size": "36.2MB"
             * */

            
            // store the Id
            fileEntry.Name = driveModel.Id;

            if (driveModel is FileModel)
            {
                var fileModel = driveModel as FileModel;
                //store the properties
                fileEntry.SetPropertyValue(PropFileName, fileModel.Title);
                fileEntry.SetPropertyValue(PropMimeType, fileModel.MimeType);
                fileEntry.SetPropertyValue(PropParents, fileModel.Parents);

                // set the size
                fileEntry.Length = Convert.ToInt64(fileModel.QuotaBytesUsed);

                // set the modified time
                fileEntry.Modified = fileModel.ModifiedDate;
            }
            


            // go ahead
            return true;
        }

        private static Boolean BuildDirectoryEntry(BaseDirectoryEntry dirEntry, IBaseModel driveModel, IStorageProviderService service, IStorageProviderSession session)
        {
            // build the file entry part 
            if (!BuildFileEntry(dirEntry, driveModel))
                return false;



            // remove all childs
            dirEntry.ClearChilds();

            Int32 code;

            WebRequest requestChildren = OAuth2.OAuth2Service.GetWebRequest(String.Format(GoogleDriveStorageProviderService.DriveListChildren, driveModel.Id), "GET", session.SessionToken as GoogleDriveTokenStorage, null);

            ChildList childList = OAuth2.OAuth2Service.ExecuteWebRequest<ChildList>(requestChildren, out code);

            if (code != (int)HttpStatusCode.OK)
            {
                HttpException hex = new HttpException(Convert.ToInt32(code), "HTTP Error");

                throw new SharpBoxException(SharpBoxErrorCodes.ErrorCouldNotRetrieveDirectoryList, hex);
            }
            else
            {
                foreach (ChildModel child in childList.Items)
                {
                    BaseFileEntry childEntry = new BaseFileEntry("Name", 0, DateTime.Now, service, session);
                    BuildFileEntry(childEntry, child);
                    dirEntry.AddChild(childEntry);
                }

            }


            // set the length
            dirEntry.Length = dirEntry.Count;

            // go ahead
            return true;
        }


    }



}
